action :mount do  
  m = data_bag_item(new_resource.data_bag, new_resource.export_id)
  unless m.nil? then
    opts = m['options'].nil? || m['options'].empty? ? "defaults" : m['options']

    directory new_resource.mount_point do
      recursive true
      action :create
    end
        
    mount new_resource.mount_point do
      device "#{m['server']}:#{m['path']}"
      fstype "nfs"
      options opts
      action [:mount, :enable]
    end
  end
end

action :unmount do
  m = data_bag_item(new_resource.data_bag, new_resource.export_id)
  unless m.nil? then
    opts = m['options'].nil? || m['options'].empty? ? "defaults" : m['options']
    
    mount new_resource.mount_point do
      device "#{m['server']}:#{m['path']}"
      fstype "nfs"
      options opts
      action [:umount, :disable]
    end
  end
end
