actions :mount, :unmount

default_action :mount

attribute :export_id, :name_attribute => true, :kind_of => String,
            :required => true
attribute :mount_point , :kind_of => String, :required => true
attribute :data_bag, :kind_of => String, :default => "nfs_exports"

attr_accessor :exists
