# est-mount

Provides an est_mount resource that simplifies managing NFS volumes on a node.  

## Usage
 
To include the LWRPs in your cookbook, use:
 
    include_recipe "est-mount"

### Adding a mount 
	est_mount "ome-axio" do
	  mount_point "/mnt/axio"
	end
  	 
### Removing a mount 
	est_mount "ome-axio" do
	  action :unmount
	end


## The nfs_exports data bag

If your chef server is provided by EST then an nfs_exports data bag will already exist.  If this isn't the case create one via the following command.
 
    knife data bag create nfs_export
 
A sample item from the nfs_export data bag

	{
        "id":"ome-axio",
        "server":"cnfs-0.campus.ksu.edu",
        "path":"/export/ome/axio",
        "options":"fstype=nfs,soft,intr,rsize=32768,wsize=32768"
	}


# Author

Author:: EST (<estech@ksu.edu>)
