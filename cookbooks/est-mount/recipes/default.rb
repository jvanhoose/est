#
# Cookbook Name:: est-mount
# Recipe:: default
#
# Copyright (C) 2013 YOUR_NAME
# 
# All rights reserved - Do Not Redistribute
#

package "nfs-utils" do
	action :upgrade
end

service "rpcbind" do
	action [ :enable, :start ]
end

service "nfslock" do
	action [ :enable, :start ]
end

node['mount_points'].each do |point|
  est_mount "home_directories" do
    mount_point "#{point['mount_point']}"
  end
end
