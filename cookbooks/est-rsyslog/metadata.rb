name             'est-rsyslog'
maintainer       'EST'
maintainer_email 'estect@ksu.edu'
license          'All rights reserved'
description      'Installs/Configures est-rsyslog'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.1'
depends          'rsyslog'