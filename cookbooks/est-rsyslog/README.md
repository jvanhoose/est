est-rsyslog cookbook
===================
A wrapper for the community rsyslog cookbook.  It is responsible for the following

* Installs appropriate rsyslog packages on the host (via upstream cookbook)
* Configures rsyslog to send all logs to the KSU default loghost


Requirements
------------
* rsyslog


Attributes
----------
Same as community cookbook.  See References below.


Usage
-----
#### est-rsyslog::default
##### Method 1: Use include_recipe inside a recipe
```ruby
{
  include_recipe "est-rsyslog"
}
```

##### Method 2: Include `est-rsyslog` in your `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[est-rsyslog]"
  ]
}
```



Contributing
------------
This is a private repository

1. Fork the repository
2. Create a named feature branch (like `add_component_x`)
3. Write you change
4. Write tests for your change
5. Run the tests, ensuring they all pass
6. Submit a Pull Request


License and Authors
-------------------
* James Thompson (<jamest@ksu.edu>)


References
----------
* Upstream cookbook - https://github.com/opscode-cookbooks/rsyslog

