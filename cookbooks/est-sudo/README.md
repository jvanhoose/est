# est-sudo cookbook
Simple wrapper around the opscode sudo cookbook that adjusts the default attributes

# Requirements
* sudo - https://github.com/opscode-cookbooks/sudo

# Usage

#### Basic LWRP usage
<code>
    sudo 'username' do
      user      'username'
      runas     'ALL'
      commands ['ALL']
    end    
</code>

Additional details : See upstream cookbook

# Attributes

# Recipes

# Author

Author:: EST (<estech@ksu.edu>)
