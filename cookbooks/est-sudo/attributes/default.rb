default['authorization']['sudo']['include_sudoers_d'] = true
default['authorization']['sudo']['sudoers_defaults']  = ['!lecture,tty_tickets,fqdn,!root_sudo,!mail_always,!mail_badpass,!mail_no_host,!mail_no_perms,!mail_no_user,log_host,log_year,log_input']
