#
# Cookbook Name:: est-sudo
# Recipe:: default
#
# Copyright (C) 2013 EST
# 
# All rights reserved - Do Not Redistribute
#
include_recipe 'sudo'

sudo 'system_default' do
  user      "ALL"    # or a username
  runas     'root'   # or 'app_user:tomcat'
  commands  ['ALL,!/bin/bash,!/bin/sh,!/bin/tcsh,!/bin/ksh']
  nopasswd true
end

