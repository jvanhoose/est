#
# Cookbook Name:: est-base
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
bash "Set proxy environment variables" do
  user "root"
  cwd "/tmp"
  not_if "grep -q http_proxy /etc/profile"
  code <<-EOH
    echo 'export http_proxy="http://www-proxy.ksu.edu:8080"' >> /etc/profile
  EOH
end

