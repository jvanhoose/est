#
# Cookbook Name:: est-nagios
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
bash "Install Nagios RPEL" do
  user "root"
  cwd "/tmp"
  not_if "rpm -q -a | grep epel"
  code <<-EOH
    rpm -Uvh http://download.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
  EOH
  environment 'http_proxy'=>"http://www-proxy.ksu.edu:8080"
end

package "nrpe" do
  action :install
end

package "nagios-plugins" do
  action :install
end

package "nagios-plugins-ifstatus" do
  action :install
end

template '/etc/nagios/nrpe.cfg' do
  source 'nrpe.cfg.erb'
  owner 'root'
  group 'root'
  mode '0644'
end

bash "Fix hosts.allow file for nagios" do
  user "root"
  cwd "/tmp"
  not_if "grep -q nrpe /etc/hosts.allow"
  code <<-EOH
    echo "nrpe: watcher.cc.ksu.edu" >> /etc/hosts.allow
  EOH
end

service 'nrpe' do
  supports :status => true, :restart => true, :reload => true
  action [:enable, :start]
end

