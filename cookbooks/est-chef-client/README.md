# est-chef-client cookbook
Simple wrapper around the Opscode chef-client cookbook.

* Overides the upsteam attributes with our default settings.

# Requirements
* chef-client - http://community.opscode.com/cookbooks/chef-client

# Usage

####Applying this cookbook during knife bootstrap.


    knife bootstrap IP_ADDRESS -r 'recipe[chef-client]' -x ubuntu -P PASSWORD --sudo
    

####Manually triggering a chef-client run from the host
While it's not a feature of this cookbook, it is handy to know how to trigger a chef-client run on a host running chef-client in daemon mode.

    killall -USR1 chef-client
    
# Attributes
Same as community cookbook.  The following attributes have been altered from upstream.

node["chef_client"]["interval"] - Sets Chef::Config[:interval] via command-line option for number of seconds between chef-client daemon runs. Default 600.  (upstream: 1800)
node["chef_client"]["splay"] - Sets Chef::Config[:splay] via command-line option for a random amount of seconds to add to interval. Default 30. (upstream: 20)

# Recipes
Same as community cookbook.

# Author

Author:: EST (<estech@ksu.edu>)
