name             'est-chef-client'
maintainer       'EST'
maintainer_email 'estech@ksu.edu'
license          'All rights reserved'
description      'Installs/Configures est-chef-client'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends  'chef-client'