#------------------------------------------------------------------------------
# ssh config group
#------------------------------------------------------------------------------
default['openssh']['client']['host'] = "*"
default['openssh']['client']['hash_known_hosts'] = "yes"
default['openssh']['client']['gssapi_authentication'] = "yes"
default['openssh']['client']['gssapi_delegate_credentials'] = "no"
default['openssh']['client']['send_env'] = "LANG LC_*"

#------------------------------------------------------------------------------
#sshd config group
#------------------------------------------------------------------------------
default['openssh']['server']['port'] = "22"
default['openssh']['server']['protocol'] = "2"
default['openssh']['server']['host_key'] = ["/etc/ssh/ssh_host_rsa_key",
  "/etc/ssh/ssh_host_dsa_key"
]
default['openssh']['server']['x11_forwarding'] = "yes"
default['openssh']['server']['key_regeneration_interval'] = "3600"
default['openssh']['server']['server_key_bits'] = "768"
default['openssh']['server']['syslog_facility'] = "AUTH"
default['openssh']['server']['log_level'] = "INFO"
default['openssh']['server']['login_grace_time'] = "120"
default['openssh']['server']['permit_root_login'] = "yes"
default['openssh']['server']['strict_modes'] = "yes"
default['openssh']['server']['rsa_authentication'] = "yes"
default['openssh']['server']['pub_key_authentication'] = "yes"
default['openssh']['server']['rhosts_rsa_authentication'] = "no"
default['openssh']['server']['host_based_authentication'] = "no"
default['openssh']['server']['ignore_user_known_hosts'] = "no"
default['openssh']['server']['ignore_rhosts'] = "yes"
default['openssh']['server']['permit_empty_passwords'] = "no"
default['openssh']['server']['challenge_response_authentication'] = "no"
default['openssh']['server']['use_p_a_m'] = "yes"
default['openssh']['server']['print_motd'] = "no"
default['openssh']['server']['print_lastlog'] = "yes"
default['openssh']['server']['t_c_p_keep_alive'] = "yes"
default['openssh']['server']['use_privilege_separation'] = "yes"
default['openssh']['server']['subsystem'] =     "sftp   /usr/libexec/sftp-server"
default['openssh']['server']['accept_env']= "LANG LC_*"

if node.role?("ome")
  default['openssh']['server']['allow_tcp_forwarding'] = "yes"
  default['openssh']['server']['client_alive_interval'] = "300"
end
