name             'est-ssh'
maintainer       'EST'
maintainer_email 'estech@ksu.edu'
license          'All rights reserved'
description      'Installs/Configures est-ssh'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.2'
depends          "openssh"
