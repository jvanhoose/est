#
# Cookbook Name:: est-firewall
# Recipe:: default
#
# Copyright (C) 2013 EST
# 
# All rights reserved - Do Not Redistribute
#

iptables_bin = "/sbin/iptables"
service_bin = "/sbin/service"

# Allow backups
#firewall_rule 'allow EST backups' do
#  ports     [22, 13724, 13782, 13783]
#  source    '129.130.253.208/29'
#  protocol  :tcp
#  action    :allow
#end

node['firewall_rules'].each do |rule|
  execute "add iptables rule to #{rule['action']} #{rule['protocol']} traffic on port #{rule['port']} from #{rule['source']}" do
    check1='|grep "\-\-dport '+"#{rule['port']}"+'"'
    check2='|grep "\-p '+"#{rule['protocol']}"+'"'
    check3='|grep "\-j '+"#{rule['action']}"+'"'
    cmd = 'iptables-save | grep "\-A INPUT"'+check1+check2+check3
    if "#{rule['source']}"!="*" 
      check4='|grep "\-s '+"#{rule['source']}"+'"'
      cmd=cmd+check4 
      command "#{iptables_bin} -A INPUT -s #{rule['source']} -p #{rule['protocol']} --dport #{rule['port']} -j #{rule['action']}"
    else
      command "#{iptables_bin} -A INPUT -p #{rule['protocol']} --dport #{rule['port']} -j #{rule['action']}"
    end
    not_if cmd
    notifies :run, "execute[save-iptables-rules]"
  end
end

execute "save-iptables-rules" do
  command "#{service_bin} iptables save"
  action :nothing
end
