#
# Cookbook Name:: est-oracledb
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
include_recipe 'yumgroup'

yumgroup "X Window System" do
  action :install
  not_if "yum grouplist | sed -e '/Available Groups/,$d' | grep 'X Window System' | grep -v 'Legacy'"
end

yumgroup "Desktop" do
  action :install
  not_if "yum grouplist | sed -e '/Available Groups/,$d' | grep 'Desktop' | grep -v 'General Purpose Desktop'"
end

package 'cloog-ppl'
package 'compat-libcap1'
package 'compat-libstdc++-33'
package 'cpp'
package 'gcc'
package 'gcc-c++'
package 'glibc-devel'
package 'glibc-headers'
package 'kernel-headers'
package 'libXmu'
package 'libXt'
package 'ksh'
package 'libXxf86misc'
package 'libXxf86vm'
package 'libaio-devel'
package 'libdmx'
package 'libstdc++-devel'
package 'mpfr'
package 'make'
package 'ppl'
package 'xorg-x11-utils'
package 'xorg-x11-xauth'
package 'libXv'
package 'libXxf86dga'
package 'bc'

ruby_block "Virtual Memory Kernel Parameters - vm.swappiness" do
  block do
    rc=Chef::Util::FileEdit.new("/etc/sysctl.conf")
    rc.insert_line_if_no_match(/vm.swappiness\s*=\s*/,"vm.swappiness = 0")
    rc.search_file_replace_line(/vm.swappiness\s*=\s*/,"vm.swappiness = 0")
    rc.write_file
  end
  not_if "grep 'vm.swappiness = 0' /etc/sysctl.conf"
end

ruby_block "Virtual Memory Kernel Parameters - vm.dirty_background_ratio" do
  block do
    rc=Chef::Util::FileEdit.new("/etc/sysctl.conf")
    rc.insert_line_if_no_match(/vm.dirty_background_ratio\s*=\s*/,"vm.dirty_background_ratio = 3")
    rc.search_file_replace_line(/vm.dirty_background_ratio\s*=\s*/,"vm.dirty_background_ratio = 3")
    rc.write_file
  end
  not_if "grep 'vm.dirty_background_ratio = 3' /etc/sysctl.conf"
end

ruby_block "Virtual Memory Kernel Parameters - vm.dirty_ratio" do
  block do
    rc=Chef::Util::FileEdit.new("/etc/sysctl.conf")
    rc.insert_line_if_no_match(/vm.dirty_ratio\s*=\s*/,"vm.dirty_ratio = 80")
    rc.search_file_replace_line(/vm.dirty_ratio\s*=\s*/,"vm.dirty_ratio = 80")
    rc.write_file
  end
  not_if "grep 'vm.dirty_ratio = 80' /etc/sysctl.conf"
end

ruby_block "Virtual Memory Kernel Parameters - vm.dirty_expire_centisecds" do
  block do
    rc=Chef::Util::FileEdit.new("/etc/sysctl.conf")
    rc.insert_line_if_no_match(/vm.dirty_expire_centisecs\s*=\s*/,"vm.dirty_expire_centisecs = 500")
    rc.search_file_replace_line(/vm.dirty_expire_centisecs\s*=\s*/,"vm.dirty_expire_centisecs = 500")
    rc.write_file
  end
  not_if "grep 'vm.dirty_expire_centisecs = 500' /etc/sysctl.conf"
end

ruby_block "Virtual Memory Kernel Parameters - vm.dirty_writeback_centisecs" do
  block do
    rc=Chef::Util::FileEdit.new("/etc/sysctl.conf")
    rc.insert_line_if_no_match(/vm.dirty_writeback_centisecs\s*=\s*/,"vm.dirty_writeback_centisecs = 100")
    rc.search_file_replace_line(/vm.dirty_writeback_centisecs\s*=\s*/,"vm.dirty_writeback_centisecs = 100")
    rc.write_file
  end
  not_if "grep 'vm.dirty_writeback_centisecs = 100' /etc/sysctl.conf"
end

def page_size
  `getconf PAGE_SIZE`.to_i
end
def memory_size
  `cat /proc/meminfo | grep MemTotal | cut -d":" -f2 | sed -e"s/\s//g" | sed -e"s/kB//g"`.to_i
end
def total_pages
  (memory_size.to_i / page_size.to_i)
end
def shared_size
  (memory_size.to_i / 2)
end
node.default['est-oracledb']['shmmax']=shared_size
node.default['est-oracledb']['shmall']=total_pages

ruby_block "Shared Memory Kernel Parameters - kernel.shmmax" do
  block do
    rc=Chef::Util::FileEdit.new("/etc/sysctl.conf")
    rc.insert_line_if_no_match(/kernel.shmmax\s*=\s*/,"kernel.shmmax = #{node.default['est-oracledb']['shmmax']}")
    rc.search_file_replace_line(/kernel.shmmax\s*=\s*/,"kernel.shmmax = #{node.default['est-oracledb']['shmmax']}")
    rc.write_file
  end
  not_if "grep 'kernel.shmmax = #{node.default['est-oracledb']['shmmax']}' /etc/sysctl.conf"
end

ruby_block "Shared Memory Kernel Parameters - kernel.shmall" do
  block do
    rc=Chef::Util::FileEdit.new("/etc/sysctl.conf")
    rc.insert_line_if_no_match(/kernel.shmall\s*=\s*/,"kernel.shmall = #{node.default['est-oracledb']['shmall']}")
    rc.search_file_replace_line(/kernel.shmall\s*=\s*/,"kernel.shmall = #{node.default['est-oracledb']['shmall']}")
    rc.write_file
  end
  not_if "grep 'kernel.shmall = #{node.default['est-oracledb']['shmall']}' /etc/sysctl.conf"
end

ruby_block "Shared Memory Kernel Parameters - kernel.shmmni" do
  block do
    rc=Chef::Util::FileEdit.new("/etc/sysctl.conf")
    rc.insert_line_if_no_match(/kernel.shmmni\s*=\s*/,"kernel.shmmni = 4096")
    rc.search_file_replace_line(/kernel.shmmni\s*=\s*/,"kernel.shmmni = 4096")
    rc.write_file
  end
  not_if "grep 'kernel.shmmni = 4096' /etc/sysctl.conf"
end

ruby_block "Semaphore Kernel Parameters" do
  block do
    rc=Chef::Util::FileEdit.new("/etc/sysctl.conf")
    rc.insert_line_if_no_match(/kernel.sem\s*=\s*/,"kernel.sem = 250 32000 100 128")
    rc.search_file_replace_line(/kernel.sem\s*=\s*/,"kernel.sem = 250 32000 100 128")
    rc.write_file
  end
  not_if "grep 'kernel.sem = 250 32000 100 128' /etc/sysctl.conf"
end

ruby_block "Network Settings Kernel Parameters - net.ipv4.ip_local_port_range" do
  block do
    rc=Chef::Util::FileEdit.new("/etc/sysctl.conf")
    rc.insert_line_if_no_match(/net.ipv4.ip_local_port_range\s*=\s*/,"net.ipv4.ip_local_port_range = 9000 65000")
    rc.search_file_replace_line(/net.ipv4.ip_local_port_range\s*=\s*/,"net.ipv4.ip_local_port_range = 9000 65000")
    rc.write_file
  end
  not_if "grep 'net.ipv4.ip_local_port_range = 9000 65000' /etc/sysctl.conf"
end

ruby_block "Network Settings Kernel Parameters - net.core.rmem_default" do
  block do
    rc=Chef::Util::FileEdit.new("/etc/sysctl.conf")
    rc.insert_line_if_no_match(/net.core.rmem_default\s*=\s*/,"net.core.rmem_default = 262144")
    rc.search_file_replace_line(/net.core.rmem_default\s*=\s*/,"net.core.rmem_default = 262144")
    rc.write_file
  end
  not_if "grep 'net.core.rmem_default = 262144' /etc/sysctl.conf"
end

ruby_block "Network Settings Kernel Parameters - net.core.rmem_max" do
  block do
    rc=Chef::Util::FileEdit.new("/etc/sysctl.conf")
    rc.insert_line_if_no_match(/net.core.rmem_max\s*=\s*/,"net.core.rmem_max = 4194304")
    rc.search_file_replace_line(/net.core.rmem_max\s*=\s*/,"net.core.rmem_max = 4194304")
    rc.write_file
  end
  not_if "grep 'net.core.rmem_max = 4194304' /etc/sysctl.conf"
end

ruby_block "Network Settings Kernel Parameters - net.core.wmem_default" do
  block do
    rc=Chef::Util::FileEdit.new("/etc/sysctl.conf")
    rc.insert_line_if_no_match(/net.core.wmem_default\s*=\s*/,"net.core.wmem_default = 262144")
    rc.search_file_replace_line(/net.core.wmem_default\s*=\s*/,"net.core.wmem_default = 262144")
    rc.write_file
  end
  not_if "grep 'net.core.wmem_default = 262144' /etc/sysctl.conf"
end

ruby_block "Network Settings Kernel Parameters - net.core.wmem_max" do
  block do
    rc=Chef::Util::FileEdit.new("/etc/sysctl.conf")
    rc.insert_line_if_no_match(/net.core.wmem_max\s*=\s*/,"net.core.wmem_max = 1048576")
    rc.search_file_replace_line(/net.core.wmem_max\s*=\s*/,"net.core.wmem_max = 1048576")
    rc.write_file
  end
  not_if "grep 'net.core.wmem_max = 1048576' /etc/sysctl.conf"
end

ruby_block "I/O Kernel Parameters - fs.aio-max-nr" do
  block do
    rc=Chef::Util::FileEdit.new("/etc/sysctl.conf")
    rc.insert_line_if_no_match(/fs.aio-max-nr\s*=\s*/,"fs.aio-max-nr = 1048576")
    rc.search_file_replace_line(/fs.aio-max-nr\s*=\s*/,"fs.aio-max-nr = 1048576")
    rc.write_file
  end
  not_if "grep 'fs.aio-max-nr = 1048576' /etc/sysctl.conf"
end

ruby_block "I/O Kernel Parameters - fs.file_max" do
  block do
    rc=Chef::Util::FileEdit.new("/etc/sysctl.conf")
    rc.insert_line_if_no_match(/fs.file_max\s*=\s*/,"fs.file_max = 6815744")
    rc.search_file_replace_line(/fs.file_max\s*=\s*/,"fs.file_max = 6815744")
    rc.write_file
  end
  not_if "grep 'fs.file_max = 6815744' /etc/sysctl.conf"
end

group "oinstall" do
  action :create
  gid 1000
  not_if "cat /etc/group | grep oinstall | grep 1000"
end

user "oracle" do
  action :create
  uid 1010
  shell "/bin/bash"
  gid 1000
end

user "grid" do
  action :create
  uid 1011
  shell "/bin/bash"
  gid 1000
end

group "dba" do
  action :create
  members ["oracle","grid"]
  append true
end

group "oper" do
  action :create
  members ["oracle","grid"]
  append true
end

group "asmdba" do
  action :create
  members ["oracle","grid"]
  append true
end

group "asmoper" do
  action :create
  members ["oracle","grid"]
  append true
end

group "asmadmin" do
  action :create
  members ["oracle","grid"]
  append true
end

group "oinstall" do
  gid 1000
  action :manage
  members ["oracle","grid"]
  append true
  not_if "cat /etc/group | grep oinstall | grep 1000 | grep 'oracle,grid'"
end

template "/etc/security/limits.d/99-grid-oracle-limits.conf" do
  source "99-grid-oracle-limits.conf.erb"
  owner "root"
  group "root"
  mode "0644"
end

template "/etc/profile.d/oracle-grid.sh" do
  source "oracle-grid.sh.erb"
  owner "root"
  group "root"
  mode "0644"
end

bash "Subscribe to RedHat Supplementary Channel" do
  user "root"
  cwd "/tmp"
  not_if "rhn-channel -l | grep rhel-x86_64-server-supplementary-6"
  code <<-EOH
    rhn-channel --add --channel=rhel-x86_64-server-supplementary-6 --user=ksu-est --password=cnsunix
  EOH
end

remote_file "/tmp/oracleasmlib-2.0.4-1.el6.x86_64.rpm" do
  source "http://download.oracle.com/otn_software/asmlib/oracleasmlib-2.0.4-1.el6.x86_64.rpm"
  mode '0644'
  checksum "61e35c9992507463a7ca055123efdfb5e960debcce33574c28f5a637d46c8173"
end

remote_file "/tmp/oracleasm-support-2.1.8-1.el6.x86_64.rpm" do
  source "http://oss.oracle.com/projects/oracleasm-support/dist/files/RPMS/rhel6/amd64/2.1.8/oracleasm-support-2.1.8-1.el6.x86_64.rpm"
  mode '0644'
  checksum "9bc5f7826e6957a2c61ace934a2f4ba1f0ee25e4a3077852305fea14f139bd00"
end

package "kmod-oracleasm"
package "oracleasmlib" do
  source "/tmp/oracleasmlib-2.0.4-1.el6.x86_64.rpm"
end

package "oracleasm-support" do
  source "/tmp/oracleasm-support-2.1.8-1.el6.x86_64.rpm"
end

execute "Configure ASM parameters" do
  command "oracleasm configure -u grid -g asmadmin -e -s y -v"
  not_if "oracleasm status | grep 'loaded: yes'"
end

package "tuned"
service "tuned" do
  action [:enable, :start ]
end

directory "/etc/tune-profiles/enterprise-storage-no-thp" do
  owner "root"
  group "root"
  mode '0755'
  action :create
end

cookbook_file "/etc/tune-profiles/enterprise-storage-no-thp/ktune.sh" do
  source "ktune.sh"
  owner "root"
  group "root"
  mode "0755"
end

cookbook_file "/etc/tune-profiles/enterprise-storage-no-thp/ktune.sysconfig" do
  source "ktune.sysconfig"
  owner "root"
  group "root"
  mode "0644"
end

cookbook_file "/etc/tune-profiles/enterprise-storage-no-thp/sysctl.ktune" do
  source "sysctl.ktune"
  owner "root"
  group "root"
  mode "0644"
end

cookbook_file "/etc/tune-profiles/enterprise-storage-no-thp/tuned.conf" do
  source "tuned.conf"
  owner "root"
  group "root"
  mode "0644"
end

execute "Select Kernel Tuning Options" do
  command "tuned-adm profile enterprise-storage-no-thp"
  not_if "tuned-adm active | grep enterprise-storage-no-thp"
end

package "unzip"
package "lsscsi"

include_recipe 'sudo'

sudo 'oracle_default' do
  user      "ALL"    # or a username
  runas     'oracle'   # or 'app_user:tomcat'
  commands  ['ALL']
  nopasswd true
end

sudo 'grid_default' do
  user      "ALL"    # or a username
  runas     'grid'   # or 'app_user:tomcat'
  commands  ['ALL']
  nopasswd true
end

